package Database;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mysql.cj.xdevapi.JsonArray;

public class DbToJson {

	final static Logger logger = Logger.getLogger(DbToJson.class);
	
	public static void main(String[] args) {

		String url = "jdbc:mysql://localhost:3306/test";
		String name = "root";
		String pass = "root";

		ArrayList<Employeee> arremp = new ArrayList<Employeee>();
		
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(url, name, pass);

			String query = "select * from employeee;";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);



			while (rs.next()) {
				
				int emp_id=rs.getInt("emp_id");
				String emp_name= rs.getString("emp_name");
			int emp_sal=	rs.getInt("emp_sal");
			
			Employeee emp = new Employeee();
			
			emp.setEmp_id(emp_id);
			emp.setEmp_name(emp_name);
			emp.setEmp_sal(emp_sal);
			
			arremp.add(emp);
			}

			
			JSONArray jsonarr=new JSONArray();
			for(int i=0;i<arremp.size();i++) {
			
			Gson g=new Gson();
			String jsonstring=g.toJson(arremp.get(i));
			jsonarr.add(jsonstring);
			
			}
			File jsonfile=new File("D:\\Employee.json");
			JSONObject jsnobj = new JSONObject();
			jsnobj.put("data", jsonarr);
			
			jsnobj.toJSONString();
			
			String jsonformattedstring=	jsnobj.toJSONString().replace("\\\"", "\"");
			String jsonfinal = jsonformattedstring.replace("\"{", "{").replace("}\"", "}");
			
			System.out.println(jsonfinal);
			
			ObjectMapper om= new ObjectMapper();
			om.writeValue(jsonfile, jsonfinal);
					
					
					stmt.close();
			con.close(); /// what happens if we dont close the connections?
		} catch (ClassNotFoundException e) {
			logger.error("Driver class was not found Message:" + e.getMessage());
			e.printStackTrace();
		} catch (SQLException e) {
			logger.error("Sql query syntax error Message:" + e.getMessage());
			e.printStackTrace();
		
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
