package Database;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class JsonToDb {

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException, ParseException, ClassNotFoundException, SQLException  {
		//ObjectMapper om = new ObjectMapper();
		JSONParser jsonParser = new JSONParser();
		//File jsonFile=new File("D:\\a.json");
		//ArrayList<Employeee> emp1 = new ArrayList<Employeee>();//databse  --->json file , json array  --- > data base
		//List<Employeee> empjson= Arrays.asList(om.readValue(jsonFile, Employeee[].class));
	 JSONObject jsonobj=	(JSONObject)jsonParser.parse(new FileReader("D:\\employee2.json"));
		//System.out.println(empjson);
		
			//System.out.println(empjson.get(i));
		//	JSONObject jsonobj = (JSONObject)jsonParser.parse(new FileReader("empjson.get(i)"));
			JSONArray jsonarr = (JSONArray)jsonobj.get("data");
			save(jsonarr);
			
			
		
	}

	 static void save(JSONArray jsonarr) throws ClassNotFoundException, SQLException {
		
		
		
			
			String url = "jdbc:mysql://localhost:3306/test";
			String name = "root";
			String pass = "root";
		  Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(url, name, pass);

			//String query = "insert into  employeee(emp_id,emp_name,emp_sal) values (?,?,?);";
			
			PreparedStatement ps = con.prepareStatement("insert into  employeee values (?,?,?);");
			for(Object obj: jsonarr)
			{
			JSONObject record =	(JSONObject)obj;
			long id = (long) record.get("emp_id");
			String name1=(String) record.get("emp_name");
			long sal = (long)record.get("emp_sal");
			
			ps.setInt(1,(int) id);
			ps.setString(2, name1);
			ps.setInt(3, (int) sal);
			ps.executeUpdate();
		
	}

}
}